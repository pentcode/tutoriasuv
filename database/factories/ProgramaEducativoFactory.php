<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ProgramaEducativo;
use Faker\Generator as Faker;

$factory->define(ProgramaEducativo::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name,
    ];
});
