<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutorias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('lugar');
            $table->date('fecha');
            $table->unsignedBigInteger('maestro_id');
            $table->unsignedBigInteger('alumno_id');
            $table->integer('sesion');

            $table->unsignedBigInteger('programa_educativo_id');
            
            $table->foreign('programa_educativo_id')
                ->references('id')->on('programa_educativos')
                ->onDelete('cascade');
            
            $table->foreign('maestro_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            
            $table->foreign('alumno_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutorias');
    }
}
