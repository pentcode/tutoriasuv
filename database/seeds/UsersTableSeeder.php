<?php
use App\User; 
use App\ProgramaEducativo; 
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;

//Rol del administrador
use Caffeinated\Shinobi\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Ejecuta el factory de los usuarios
    	factory(App\User::class ,9)->create();	

    	Role::create([
    		'name'		=> 'Admin',
    		'slug'		=> 'admin',
    		'special'	=> 'all-access'
        ]);
        $maestros = Role::create([
    		'name'		=> 'Maestros',
    		'slug'		=> 'maestros',
        ]);
        $alumnos = Role::create([
    		'name'		=> 'Alumnos',
    		'slug'		=> 'alumnos',
        ]);
        $coordinadores = Role::create([
    		'name'		=> 'Coordinadores',
    		'slug'		=> 'coordinadores',
        ]);

        $alumnos->givePermissionTo(
            'tutorias.index',
            'evaluaciones.create',
            'evaluaciones.edit',
        );
        $maestros->givePermissionTo(
            'users.index',
            'users.show',            
            'tutorias.index',
            'tutorias.create',
            'tutorias.show',
            'tutorias.destroy',
            'tutorias.edit'
        );
        $coordinadores->givePermissionTo(
            'users.index',
            'users.show',
            'tutorias.index',
            'tutorias.create',
            'tutorias.show',
            'tutorias.destroy',
            'tutorias.edit',
            'educativos.show',
            'educativos.index',
            'educativos.create',
            'educativos.edit',
            'educativos.destroy',

        );


        $pe = ProgramaEducativo::create ([
            'nombre'    => 'Redes y Servicios de Computo'
        ]);

        $admin = User::create([
            'name'		=> 'admin',
            'email'     => 'admin@gmail.com',
            'email_verified_at' => Carbon::now(),
            'password'  => Hash::make('12345678'),
        ]);

        $admin->assignRoles('admin');

        $alumno = User::create([
            'name'		=> 'alumno',
            'email'     => 'alumno@gmail.com',
            'email_verified_at' => Carbon::now(),
            'password'  => Hash::make('12345678'),
            'programa_educativo_id' => $pe->id,
        ]);

        $alumno->assignRoles('alumnos');

        $maestro = User::create([
            'name'		=> 'maestro',
            'email'     => 'maestro@gmail.com',
            'email_verified_at' => Carbon::now(),
            'password'  => Hash::make('12345678'),
        ]);

        $maestro->assignRoles('maestros');

        $coordinador = User::create([
            'name'		=> 'coordinador',
            'email'     => 'coordinador@gmail.com',
            'email_verified_at' => Carbon::now(),
            'password'  => Hash::make('12345678'),
        ]);

        $coordinador->assignRoles('coordinadores');

        

        for ($i = 1; $i<4; $i++){
            $l = User::find($i);
            
            $l->programa_educativo_id = $pe->id;
            $l->save();
            $l->assignRoles('alumnos');

        }

        for ($i = 4; $i<7; $i++){
            User::find($i)->assignRoles('maestros');
        }

        for ($i = 7; $i<10; $i++){
            User::find($i)->assignRoles('coordinadores');
        }
    }
}
