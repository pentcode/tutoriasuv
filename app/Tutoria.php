<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tutoria extends Model
{
    protected $fillable = [
        'lugar','fecha','maestro_id','sesion','programa_educativo_id','alumno_id',
    ];

    public function maestro (){
        return $this->belongsTo('App\User','maestro_id');
    }
    public function alumno (){
        return $this->belongsTo('App\User','alumno_id');
    }
    public function programa_educativo (){
        return $this->belongsTo('App\ProgramaEducativo','programa_educativo_id');
    }
    public function evaluacion(){
        return $this->hasOne('App\Evaluacion','tutoria_id');
    }
}
