<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluacion extends Model
{
    protected $fillable = [
        'descripcion','tutoria_id',
    ];

    public function tutoria (){
        return $this->belongsTo('App\Tutoria','tutoria_id');
    }
    
}
