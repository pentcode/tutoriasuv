<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramaEducativo extends Model
{
     protected $fillable = [
        'nombre'
    ];
}
