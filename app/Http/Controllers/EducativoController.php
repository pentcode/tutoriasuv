<?php

namespace App\Http\Controllers;
use App\ProgramaEducativo;
use Illuminate\Http\Request;

class EducativoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Se obtiene toda la informacion de la entidad educativos para mostrarlas posteriormente pasarlos en una varible y mostrarlos en una vista
        $educativos = ProgramaEducativo::paginate();
        
        return view('educativos.index', compact('educativos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('educativos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'nombre' => 'required|max:100',
        ]);

        //Guarda la tutoría creada en la variable
        $educativo = ProgramaEducativo::create($data);

        return redirect()->route('educativos.edit', $educativo->id)
            ->with('info', 'Programa Educativo guardado correctamente');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Educativo  $educativo
     * @return \Illuminate\Http\Response
     */

    //Realiza la búsqueda automaticamente del id de la tutorías
    public function show(ProgramaEducativo $educativo)
    {
        //dd($educativo->id);
        return view('educativos.show', compact('educativo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Educativo  $educativo
     * @return \Illuminate\Http\Response
     */
    public function edit(ProgramaEducativo $educativo)
    {  
        return view('educativos.edit', compact('educativo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Educativo  $educativo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProgramaEducativo $educativo)
    {
        $educativo->update($request->all());

        return redirect()->route('educativos.edit', $educativo->id)
            ->with('info', 'Programa Educativo actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Educativo  $educativo
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProgramaEducativo $educativo)
    {
        $educativo->delete();

        return back()->with('info', 'Programa Educativo eliminado correctamente');
    }
}


