<?php

namespace App\Http\Controllers;

use Caffeinated\Shinobi\Models\Role;
use App\ProgramaEducativo;
use App\Tutoria;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class TutoriaController extends Controller
{

    private function selects ($programaEducativoId){
        $rol = Role::where('name', 'Maestros')->first();
        $rolAlumno = Role::where('name', 'Alumnos')->first();

        $maestros = [];
        $programa = [""=>'Seleccionar'];
        $alumnos = [];

        foreach ($rol->users as $user) {
            $maestros[$user['id']] = $user['name'];
        }

        foreach (ProgramaEducativo::all() as $programaeducativo ) {
            $programa[$programaeducativo['id']] = $programaeducativo['nombre'];
        }

        if($programaEducativoId !== null){
            $todosAlumnos = Role::where('name', 'Alumnos')->first()->users()->where('programa_educativo_id', $programaEducativoId)->get();
            
            foreach($todosAlumnos as $alumno){
                $alumnos[$alumno['id']] = $alumno['name'];
            }
        }

        return compact('maestros', 'programa', 'alumnos');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //Se obtiene toda la informacion de la entidad tutorias para mostrarlas posteriormente pasarlos en una varible y mostrarlos en una vista
        
        if($request->user()->hasRole('admin') || $request->user()->hasRole('coordinadores')){
            $tutorias = Tutoria::with('maestro','alumno','programa_educativo')->paginate();
        }elseif ($request->user()->hasRole('maestros')){
            $tutorias = Tutoria::with('maestro','alumno','programa_educativo')->where('maestro_id', $request->user()->id)->paginate();
        }elseif ($request->user()->hasRole('alumnos')){
            $tutorias = Tutoria::with('maestro','alumno','programa_educativo')->where('alumno_id', $request->user()->id)->paginate();
        } else {
            $tutorias = [];
        }
        
        return view('tutorias.index', compact('tutorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tutorias.create', $this->selects(null));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'lugar' => 'required|max:100',
            'fecha' => 'required|date|after_or_equal:today',
            'maestro_id' => [
                'required',
                Rule::in(Role::where('slug','maestros')->first()->users()->get()->map(function($user){
                    return $user->id;
                })),
            ],
            'sesion' => 'required|in:1,2,3',
            'programa_educativo_id' => 'required|exists:programa_educativos,id',
            'alumno_id' => [
                'required',
                Rule::in(Role::where('slug','alumnos')->first()->users()->get()->map(function($user){
                    return $user->id;
                })),
            ],
        ]);
        //Guarda la tutoría creada en la variable
        $tutoria = Tutoria::create($data);

        return redirect()->route('tutorias.index', $tutoria->id)
            ->with('info', 'Tutoría guardada correctamente');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tutoria  $tutoria
     * @return \Illuminate\Http\Response
     */

    //Realiza la búsqueda automaticamente del id de la tutorías
    public function show(Tutoria $tutoria)
    {
        //dd($tutoria->id);
        return view('tutorias.show', compact('tutoria'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tutoria  $tutoria
     * @return \Illuminate\Http\Response
     */
    public function edit(Tutoria $tutoria)
    {
        return view('tutorias.edit', array_merge(compact('tutoria'),$this->selects($tutoria->programa_educativo_id)));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tutoria  $tutoria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tutoria $tutoria)
    {
        $tutoria->update($request->all());

        return redirect()->route('tutorias.edit', $tutoria->id)
            ->with('info', 'Tutoría actualizada correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tutoria  $tutoria
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tutoria $tutoria)
    {
        $tutoria->delete();

        return back()->with('info', 'Tutoría eliminada correctamente');
    }
}
