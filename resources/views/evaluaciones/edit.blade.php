@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Editar Evaluación</div>

                <div class="panel-body">                    
                    {!! Form::model($evaluacion, ['route' => ['evaluaciones.update', $evaluacion->id],
                    'method' => 'PUT']) !!}

                        @include('evaluaciones.partials.form')
                        
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection