@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Evaluación
                </div>

                <div class="panel-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Descripcion</th>
                                <th colspan="4">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($evaluaciones as $evaluacion)
                            <tr>
                                <td>{{ $evaluacion->descripcion }}</td>
                                @can('evaluacion.show')
                                <td width="10px">
                                    <a href="{{ route('evaluaciones.show', $evaluacion->id) }}" 
                                    class="btn btn-sm btn-default">
                                        ver
                                    </a>
                                </td>
                                @endcan
                                @can('evaluaciones.edit')
                                <td width="10px">
                                    <a href="{{ route('evaluaciones.edit', $evaluacion->id) }}" 
                                    class="btn btn-sm btn-default">
                                        editar
                                    </a>
                                </td>
                                @endcan
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $evaluaciones->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
