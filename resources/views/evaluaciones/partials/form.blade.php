<div class="form-group">
	{{ Form::label('descripcion', 'Notas de la evaluación') }}
	{{ Form::text('descripcion', null, ['class' => 'form-control', 'id' => 'descripcion']) }}
	@error('descripcion')
		<div class="alert alert-danger">{{$message}}</div>
	@enderror
</div>
<div class="form-group">
	{{ Form::submit('Guardar', ['class' => 'btn btn-sm btn-primary']) }}
</div>