@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Evaluaciones</div>

                <div class="panel-body">                                   
                    <p><strong>Evaluacion</strong>  {{ $evaluacion->descripcion }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<!--Quitar esta vista-->