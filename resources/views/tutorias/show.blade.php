@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Tutoría</div>

                <div class="panel-body">                                        
                    <p><strong>Tutoría</strong>     {{ $tutoria->sesion }}</p>
                    <p><strong>Salón</strong>  {{ $tutoria->lugar }}</p>
                    <p><strong>Fecha</strong>  {{ $tutoria->fecha }}</p>
                    <p><strong>Maestro</strong>  {{ $tutoria->maestro->name }}</p>
                    <p><strong>Programa Educativo</strong>  {{ $tutoria->programa_educativo->nombre }}</p>
                    <p><strong>Alumno</strong>  {{ $tutoria->alumno->name }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection