@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Tutorías</div>

                <div class="panel-body">                    
                    {!! Form::model($tutoria, ['route' => ['tutorias.update', $tutoria->id],
                    'method' => 'PUT']) !!}

                        @include('tutorias.partials.form')
                        
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection