@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Tutorías
                    
                    @can('tutorias.create')
                    <a href="{{ route('tutorias.create') }}" 
                    class="btn btn-sm btn-primary pull-right">
                        Crear
                    </a>
                    @endcan
                </div>

                <div class="panel-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="10px">ID</th>
                                <th>Lugar</th>
                                <th>Fecha</th>
                                <th>Maestro</th>
                                <th>Sesión</th>
                                <th>Programa Educativo</th>
                                <th>Alumno</th>
                                <th colspan="4">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($tutorias as $tutoria)
                            <tr>
                                <td>{{ $tutoria->id }}</td>
                                <td>{{ $tutoria->lugar }}</td>
                                <td>{{ $tutoria->fecha }}</td>
                                <td>{{ $tutoria->maestro->name }}</td>
                                <td>{{ $tutoria->sesion }}</td>
                                <td>{{ $tutoria->programa_educativo->nombre }}</td>
                                <td>{{ $tutoria->alumno->name }}</td>
                                @can('tutorias.show')
                                <td width="10px">
                                    <a href="{{ route('tutorias.show', $tutoria->id) }}" 
                                    class="btn btn-sm btn-default">
                                        ver
                                    </a>
                                </td>
                                @endcan
                                @can('tutorias.edit')
                                <td width="10px">
                                    <a href="{{ route('tutorias.edit', $tutoria->id) }}" 
                                    class="btn btn-sm btn-default">
                                        editar
                                    </a>
                                </td>
                                @endcan
                                @can('tutorias.destroy')
                                <td width="10px">
                                    {!! Form::open(['route' => ['tutorias.destroy', $tutoria->id], 
                                    'method' => 'DELETE']) !!}
                                        <button class="btn btn-sm btn-default">
                                            Eliminar
                                        </button>
                                    {!! Form::close() !!}
                                </td>
                                @endcan
                                @can('evaluaciones.create')
                                <td width="10px">
                                    @if(Auth::user()->hasRole('admin') && $tutoria->evaluacion)
                                        <a href="{{ route('evaluaciones.show', $tutoria->evaluacion->id) }}" 
                                            class="btn btn-sm btn-default">Ver Evaluacion
                                        </a>
                                    @else
                                        @if($tutoria->evaluacion)
                                            <a href="{{ route('evaluaciones.edit', $tutoria->evaluacion->id) }}" 
                                                class="btn btn-sm btn-default">Editar evaluacion
                                            </a>
                                        @else
                                            @if(!Auth::user()->hasRole('admin'))
                                                <a href="{{ route('evaluaciones.create', $tutoria->id) }}" 
                                                    class="btn btn-sm btn-default">Evaluar
                                                </a>
                                            @endif
                                        @endif
                                    @endif
                                </td>
                                @endcan
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $tutorias->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
