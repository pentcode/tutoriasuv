@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Programas Educativos
                    
                    @can('educativos.create')
                    <a href="{{ route('educativos.create') }}" 
                    class="btn btn-sm btn-primary pull-right">
                        Crear
                    </a>
                    @endcan
                </div>

                <div class="panel-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Nombre</th>
                                <th colspan="4">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($educativos as $educativo)
                            <tr>
                                <td>{{ $educativo->id }}</td>
                                <td>{{ $educativo->nombre }}</td>
                                @can('educativos.show')
                                <td width="10px">
                                    <a href="{{ route('educativos.show', $educativo->id) }}" 
                                    class="btn btn-sm btn-default">
                                        ver
                                    </a>
                                </td>
                                @endcan
                                @can('educativos.edit')
                                <td width="10px">
                                    <a href="{{ route('educativos.edit', $educativo->id) }}" 
                                    class="btn btn-sm btn-default">
                                        editar
                                    </a>
                                </td>
                                @endcan
                                @can('educativos.destroy')
                                <td width="10px">
                                    {!! Form::open(['route' => ['educativos.destroy', $educativo->id], 
                                    'method' => 'DELETE']) !!}
                                        <button class="btn btn-sm btn-default">
                                            Eliminar
                                        </button>
                                    {!! Form::close() !!}
                                </td>
                                @endcan
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $educativos->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
