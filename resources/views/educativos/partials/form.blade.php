<div class="form-group">
	{{ Form::label('nombre', 'Nombre del Programa Educativo') }}
	{{ Form::text('nombre', null, ['class' => 'form-control', 'id' => 'nombre']) }}
	@error('nombre')
		<div class="alert alert-danger">{{$message}}</div>
	@enderror
</div>
<div class="form-group">
	{{ Form::submit('Guardar', ['class' => 'btn btn-sm btn-primary']) }}
</div>